package Run.Test;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ArgPar.Annotation.CommandGroup;
import ArgPar.Annotation.ParsableArgument;
import ArgPar.Annotation.ParsableCommand;
import ArgPar.Annotation.ParsableFlag;
import ArgPar.Annotation.ParsableOption;
import ArgPar.Exception.ParseException;
import ArgPar.Parser.ArgumentGroupParser;
import ArgPar.Parser.ArgumentParser;

public class Tests {
    private static interface VoidOperator {
        void op();
    }

    private String[] args(String argString) {
        requireNonNull(argString);
        return argString.split(" ");
    }

    private <T> void assertEqual(T expected, T got) throws TestException {
        if (expected.equals(got)) return;
        throw new TestException(expected, got);
    }

    private <T> void assertEqualArray(T[] expected, T[] got) throws TestException {
        if (expected.length != got.length) {
            throw new TestException(expected, got);
        }
        for (int i = 0; i < expected.length; i++) {
            assertEqual(expected[i], got[i]);
        }
    }

    // @todo: code dupl
    private void assertEqualArray(int[] expected, int[] got) throws TestException {
        if (expected.length != got.length) {
            throw new TestException(expected, got);
        }
        for (int i = 0; i < expected.length; i++) {
            assertEqual(expected[i], got[i]);
        }
    }

    private <T> void assertNotEqual(T expected, T got) throws TestException {
        if (!expected.equals(got)) return;
        throw new TestException(expected, got);
    }

    private void failingTest(VoidOperator op) throws TestException {
        try {
            op.op();
            throw new TestException();
        } catch (ParseException e) {
        }
    }

    @Test
    public void test1() throws TestException {
        var parser = new ArgumentParser<Test1>(Test1.class);
        {
            var cmd = parser.parse(args("Test1 test"));
            assertEqual("test", cmd.name);
        }
        {
            var cmd = parser.parse(args("Test1 arg"));
            assertEqual("arg", cmd.name);
        }
        {
            failingTest(() -> parser.parse(args("Test")));
        }
    }

    @Test
    public void test2() throws TestException {
        var parser = new ArgumentParser<Test2>(Test2.class);
        {
            var cmd = parser.parse(args("Test2 test"));
            assertEqual("test", cmd.name);
            assertEqual(10, cmd.counter);
        }
        {
            var cmd = parser.parse(args("Test2 arg 20"));
            assertEqual("arg", cmd.name);
            assertEqual(20, cmd.counter);
        }
    }

    @Test
    public void test3() throws TestException {
        var parser = new ArgumentParser<Test3>(Test3.class);
        {
            failingTest(() -> parser.parse(args("Test3 test")));
        }
        {
            var cmd = parser.parse(args("Test3 true --flag false"));
            assertEqual(true, cmd.arg);
            assertEqual(false, cmd.flag);
        }
        {
            var cmd = parser.parse(args("Test3 true --flag true"));
            assertEqual(true, cmd.arg);
            assertEqual(true, cmd.flag);
        }
        {
            var cmd = parser.parse(args("Test3 --flag true false"));
            assertEqual(false, cmd.arg);
            assertEqual(true, cmd.flag);
        }
    }

    @Test
    public void test4() throws TestException {
        var parser = new ArgumentParser<Test4>(Test4.class);
        {
            failingTest(() -> parser.parse(args("Test4")));
            failingTest(() -> parser.parse(args("Test4 --fla true")));
            failingTest(() -> parser.parse(args("Test4 -g false")));
            failingTest(() -> parser.parse(args("Test4 --flag")));
            failingTest(() -> parser.parse(args("Test4 --flag abc")));
        }
        {
            var cmd = parser.parse(args("Test4 --flag 0"));
            assertEqual(0, cmd.flag);
        }
        {
            var cmd = parser.parse(args("Test4 -f 5555"));
            assertEqual(5555, cmd.flag);
        }
        {
            var cmd = parser.parse(args("Test4 -f=5555"));
            assertEqual(5555, cmd.flag);
        }
    }

    @Test
    public void test5() throws TestException {
        var parser = new ArgumentParser<Test5>(Test5.class);
        {
            failingTest(() -> parser.parse(args("Test5 -b true -f 0 --num 1")));
            failingTest(() -> parser.parse(args("Test5 -b true -f 0 --num 1 -m A -m B")));
            failingTest(() -> parser.parse(args("Test5 -b true -f 0")));
            failingTest(() -> parser.parse(args("Test5 --flag abc -b true --num 0 -m A")));
            failingTest(() -> parser.parse(args("Test5 -f 0 --num 0 -m A")));
        }
        {
            var cmd = parser.parse(args("Test5 -f 100 -b false --num 12 -m A"));
            assertEqual(100, cmd.flag);
            assertEqual(false, cmd.build);
            assertEqual(12, cmd.num);
            assertEqual(Test5.Mode.A, cmd.mode);
        }
        {
            var cmd = parser.parse(args("Test5 --flag 100 -b false --num 12 -m A"));
            assertEqual(100, cmd.flag);
            assertEqual(false, cmd.build);
            assertEqual(12, cmd.num);
            assertEqual(Test5.Mode.A, cmd.mode);
        }
        {
            var cmd = parser.parse(args("Test5 --num 12 --flag 100 -m B --build true"));
            assertEqual(100, cmd.flag);
            assertEqual(true, cmd.build);
            assertEqual(12, cmd.num);
            assertEqual(Test5.Mode.B, cmd.mode);
        }
        {
            var cmd = parser.parse(args("Test5 --flag 100 --build true -m B"));
            assertEqual(100, cmd.flag);
            assertEqual(true, cmd.build);
            assertEqual(10, cmd.num);
            assertEqual(Test5.Mode.B, cmd.mode);
        }
    }

    @Test
    public void test6() throws TestException {
        var parser = new ArgumentParser<Test6>(Test6.class);
        {
            var cmd = parser.parse(args("Test6"));
            assertEqual(false, cmd.flag1);
            assertEqual(false, cmd.flag2);
        }
        {
            var cmd = parser.parse(args("Test6 --flag1"));
            assertEqual(true, cmd.flag1);
            assertEqual(false, cmd.flag2);
        }
        {
            var cmd = parser.parse(args("Test6 --flag2"));
            assertEqual(false, cmd.flag1);
            assertEqual(true, cmd.flag2);
        }
        {
            var cmd = parser.parse(args("Test6 -f2"));
            assertEqual(false, cmd.flag1);
        }  
        {
            var cmd = parser.parse(args("Test6 --flag1 --flag2"));
            assertEqual(true, cmd.flag1);
            assertEqual(true, cmd.flag2);
        }
        {
            var cmd = parser.parse(args("Test6 --flag2 --flag1"));
            assertEqual(true, cmd.flag1);
            assertEqual(true, cmd.flag2);
        }
        {
            var cmd = parser.parse(args("Test6 -f2 --flag1"));
            assertEqual(true, cmd.flag1);
            assertEqual(true, cmd.flag2);
        }
    }

    @Test
    public void test7() throws TestException {
        var parser = new ArgumentGroupParser(Test7.class);
        {
            failingTest(() -> parser.parse(args(""), Object.class));
            failingTest(() -> parser.parse(args("Test5"), Object.class));
            failingTest(() -> parser.parse(args("Test5 --flag 100"), Object.class));
        }
        {
            var cmd = parser.parse(args("Test5 --flag 100 -b false --num 12 -m A"), Test5.class);
            assertEqual(100, cmd.flag);
            assertEqual(false, cmd.build);
            assertEqual(12, cmd.num);
            assertEqual(Test5.Mode.A, cmd.mode);
        }
        {
            var cmd = parser.parse(args("Test6"), Test6.class);
            assertEqual(false, cmd.flag1);
            assertEqual(false, cmd.flag2);
        }
        {
            var cmd = parser.parse(args("Test6 --flag1"), Test6.class);
            assertEqual(true, cmd.flag1);
            assertEqual(false, cmd.flag2);
        }
    }

    @Test
    public void test8() throws TestException {
        var parser = new ArgumentParser<Test8>(Test8.class);
        {
            var cmd = parser.parse(args("Test8 --nums 1,2,3,4,5"));
            Integer[] expected = {1,2,3,4,5};
            assertEqualArray(expected, cmd.numbers);
        }
        {
            var cmd = parser.parse(args("Test8 --nums=1,2,3,4,5"));
            Integer[] expected = {1,2,3,4,5};
            assertEqualArray(expected, cmd.numbers);
        }
    }

    // @todo: currently not supported (arrays have to use boxed types)
    @Test
    public void test9() throws TestException {
        var parser = new ArgumentParser<Test9>(Test9.class);
        {
            var cmd = parser.parse(args("Test9 --nums 1,2,3,4,5"));
            int[] expected = {1,2,3,4,5};
            assertEqualArray(expected, cmd.numbers);
        }
    }

    @Test
    public void test10() throws TestException {
        var parser = new ArgumentParser<Test10>(Test10.class);
        {
            var cmd = parser.parse(args("Test10 --phases LEX,SYN,IMC"));
            assertEqual(true, cmd.phases.contains(Test10.Phase.LEX));
            assertEqual(true, cmd.phases.contains(Test10.Phase.SYN));
            assertEqual(true, cmd.phases.contains(Test10.Phase.IMC));
            assertEqual(false, cmd.phases.contains(Test10.Phase.FRM));
            assertEqual(false, cmd.phases.contains(Test10.Phase.AST));
            assertEqual(false, cmd.phases.contains(Test10.Phase.SEM));
            assertEqual(false, cmd.phases.contains(Test10.Phase.INT));
        }
        {
            var cmd = parser.parse(args("Test10 --phases SYN"));
            assertEqual(true, cmd.phases.contains(Test10.Phase.SYN));
            assertEqual(false, cmd.phases.contains(Test10.Phase.LEX));
            assertEqual(false, cmd.phases.contains(Test10.Phase.IMC));
            assertEqual(false, cmd.phases.contains(Test10.Phase.FRM));
            assertEqual(false, cmd.phases.contains(Test10.Phase.AST));
            assertEqual(false, cmd.phases.contains(Test10.Phase.SEM));
            assertEqual(false, cmd.phases.contains(Test10.Phase.INT));
        }
    }

    @Test
    public void test11() throws TestException {
        var parser = new ArgumentParser<Test5>(Test5.class);
        {
            var cmd = parser.parse(args("Test5 -f=100 -b=false --num=12 -m=A"));
            assertEqual(100, cmd.flag);
            assertEqual(false, cmd.build);
            assertEqual(12, cmd.num);
            assertEqual(Test5.Mode.A, cmd.mode);
        }
        {
            var cmd = parser.parse(args("Test5 --flag=100 -b=false --num=12 -m=A"));
            assertEqual(100, cmd.flag);
            assertEqual(false, cmd.build);
            assertEqual(12, cmd.num);
            assertEqual(Test5.Mode.A, cmd.mode);
        }
        {
            var cmd = parser.parse(args("Test5 --num 12 --flag=100 -m=B --build=true"));
            assertEqual(100, cmd.flag);
            assertEqual(true, cmd.build);
            assertEqual(12, cmd.num);
            assertEqual(Test5.Mode.B, cmd.mode);
        }
        {
            var cmd = parser.parse(args("Test5 --flag=100 --build true -m=B"));
            assertEqual(100, cmd.flag);
            assertEqual(true, cmd.build);
            assertEqual(10, cmd.num);
            assertEqual(Test5.Mode.B, cmd.mode);
        }
    }

    @ParsableCommand(commandName = "Test1", description = "Command Line Application")
    public static class Test1 { 
        @ParsableArgument
        public String name;
    }

    @ParsableCommand(commandName = "Test2", description = "Command Line Application")
    public static class Test2 { 
        @ParsableArgument
        public String name;

        @ParsableArgument(defaultValue = "10")
        public int counter;
    }

    @ParsableCommand(commandName = "Test3", description = "Command Line Application")
    public static class Test3 { 
        @ParsableArgument
        public boolean arg;

        @ParsableOption(name = "--flag")
        public Boolean flag;
    }

    @ParsableCommand(commandName = "Test4", description = "Command Line Application")
    public static class Test4 { 
        @ParsableOption(name = {"--flag", "-f"})
        public Integer flag;
    }

    @ParsableCommand(commandName = "Test5", description = "Command Line Application")
    public static class Test5 { 
        public static enum Mode { A, B }

        @ParsableOption(name = {"--flag", "-f"})
        public Integer flag;

        @ParsableOption(name = {"--build", "-b"})
        public Boolean build;

        @ParsableOption(name = "--num")
        public int num = 10;

        @ParsableOption(name = "-m")
        public Mode mode;
    }

    @ParsableCommand(commandName = "Test6", description = "Command Line Application")
    public static class Test6 {
        @ParsableFlag(name = "--flag1")
        public boolean flag1;

        @ParsableFlag(name = {"--flag2", "-f2"})
        public boolean flag2;
    }

    @CommandGroup(name = "Group", description = "Command Line Interface", subcommands = {Test5.class, Test6.class})
    public static class Test7 {
    }

    @ParsableCommand(commandName = "Test8", description = "")
    public static class Test8 {
        @ParsableOption(name = "--nums")
        public Integer[] numbers;
    }

    // @todo: arrays must be backed by boxed types
    @ParsableCommand(commandName = "Test9", description = "")
    public static class Test9 {
        @ParsableOption(name = "--nums")
        public int[] numbers;
    }

    @ParsableCommand(commandName = "Test10", description = "")
    public static class Test10 {
        public static class PhasesEnumSet extends ForwardingSet<Phase> {
            PhasesEnumSet(EnumSet<Phase> set) {
                super(set);
            }

            public static PhasesEnumSet valueOf(String arg) {
                var split = arg.split(",");
                var set = new PhasesEnumSet(EnumSet.noneOf(Phase.class));
                for (var s : split) {
                    var phase = Phase.valueOf(s.trim());
                    if (phase == null) {
                        throw new IllegalArgumentException("Could not parse <Phase>!");
                    }
                    set.add(phase);
                }
                return set;
            }
        }

        public static enum Phase {
            LEX, SYN, AST, SEM, FRM, IMC, INT
        }

        @ParsableOption(name = "--phases")
        public PhasesEnumSet phases;
    }
}

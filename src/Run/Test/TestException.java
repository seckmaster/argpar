package Run.Test;

public class TestException extends Exception {
    public Object expected, got;

    public TestException(Object expected, Object got) {
        this.expected = expected;
        this.got = got;
    }

    public TestException() {}
}

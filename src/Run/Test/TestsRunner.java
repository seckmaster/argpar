package Run.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import ArgPar.Exception.ParseException;

public class TestsRunner {
    public void runTests(Class<?> tests) throws IllegalAccessException, InvocationTargetException {
        var instance = new Tests();
        var testMethods = new ArrayList<Method>();
        for (var method : tests.getMethods()) {
            var annotations = method.getDeclaredAnnotations();
            if (annotations.length > 0 && annotations[0] instanceof Test) {
                testMethods.add(method);
            }
        }

        var in = new PrintStream(new ByteArrayOutputStream());
        var out = new PrintStream(new FileOutputStream(FileDescriptor.out));

        System.out.println("Running " + testMethods.size() + " tests:");
        var succeeded = 0;
        for (var method : testMethods) {
            try {
                System.setOut(in);
                method.invoke(instance);
                System.setOut(out);

                System.out.println("✅ test '" + method.getName() + "' succeeded!");
                succeeded += 1;
            } catch (InvocationTargetException e) {
                System.setOut(out);

                if (e.getCause() instanceof TestException) {
                    var exc = (TestException) e.getCause();
                    System.out.println("❌ test '" + method.getName() + "' failed: expected '"+exc.expected+"', got '" + exc.got + "'!");
                    continue;
                }
                if (e.getCause() instanceof ParseException) {
                    var exc = (ParseException) e.getCause();
                    System.out.println("❌ test '" + method.getName() + "' failed: " + exc.toString());
                    continue;
                }
                e.printStackTrace();
            }
        }

        if (succeeded == testMethods.size()) {
            System.out.println("✅ " + succeeded + " out of " + testMethods.size() + " tests passed!");
        } else {
            System.out.println("❌ " + succeeded + " out of " + testMethods.size() + " tests passed!");
        }
    }
}

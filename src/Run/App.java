package Run;

import java.lang.reflect.InvocationTargetException;

import Run.Test.Tests;
import Run.Test.TestsRunner;

public class App {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
        if (args.length > 0 && args[0].equals("--test")) {
            var runner = new TestsRunner();
            runner.runTests(Tests.class);
            return;
        }
    }
}

package ArgPar.Exception;

public class ParseException extends RuntimeException {
    String reason;

    public ParseException(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return reason;
    }
}

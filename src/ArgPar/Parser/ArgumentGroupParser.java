package ArgPar.Parser;

import static java.util.Objects.requireNonNull;

import ArgPar.Annotation.CommandGroup;
import ArgPar.Annotation.ParsableCommand;
import ArgPar.Exception.ParseException;

public class ArgumentGroupParser {
    private CommandGroup group;

    public <Group> ArgumentGroupParser(Class<Group> groupClass) {
        requireNonNull(groupClass);
        var annotations = groupClass.getAnnotations();
        if (annotations.length == 0) {
            throw new IllegalArgumentException();
        }
        var annotation = annotations[0];
        if (!(annotation instanceof CommandGroup)) {
            throw new IllegalArgumentException();
        }
        if (((CommandGroup) annotation).subcommands().length == 0) {
            throw new IllegalArgumentException();
        }
        this.group = (CommandGroup) annotation;
    }

    public <C> C parse(String[] args, Class<C> resultClass) throws ParseException {
        if (args.length == 0) {
            System.out.println(commandGroupHelp());
            throw new ParseException("Error: Missing value for command!");
        }

        Class<?> targetCommand = null;

        for (var command : group.subcommands()) {
            var cmdAnnotations = command.getAnnotations();
            if (cmdAnnotations.length > 0 && cmdAnnotations[0] instanceof ParsableCommand) {
                var parsableCommand = (ParsableCommand) cmdAnnotations[0];
                if (parsableCommand.commandName().isEmpty()) {
                    throw new IllegalArgumentException();
                }
                if (parsableCommand.commandName().equals(args[0])) {
                    targetCommand = command;
                    break;
                }
            } else {
                throw new IllegalArgumentException();
            }
        }

        if (targetCommand == null) {
            System.out.println(commandGroupHelp());
            throw new ParseException("Error: An uknown command!");
        }

        var parser = new ArgumentParser<>(targetCommand);
        var result = parser.parse(args);
        if (resultClass.isInstance(result)) {
            @SuppressWarnings(value = "unchecked") // @note: we made sure it's safe
            var casted = (C) result;
            return casted;
        }
        throw new IllegalArgumentException();
    }

    private String commandGroupHelp() {
        var name = group.name();
        var version = group.version();
        var description = group.description();

        var builder = new StringBuilder();
        if (description != null && description.length() > 0) {
            builder.append("OVERVIEW: ");
            builder.append(description);
            builder.append("\n\n");
        }
        if (version != null && version.length() > 0) {
            builder.append("Version: ");
            builder.append(version);
            builder.append("\n");
        }
        builder.append("USAGE: ");
        builder.append(name);
        builder.append(" <subcommand>");
        builder.append("\n\n");

        builder.append("SUBCOMMANDS:\n");
        for (var subcommand : group.subcommands()) {
            var cmdAnnotations = subcommand.getAnnotations();
            if (cmdAnnotations.length > 0 && cmdAnnotations[0] instanceof ParsableCommand) {
                var parsableCommand = (ParsableCommand) cmdAnnotations[0];
                builder.append("  ");
                builder.append(parsableCommand.commandName());
                builder.append("\n");
            }
        }

        return builder.toString();
    }
}

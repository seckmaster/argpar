package ArgPar.Parser;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ArgPar.Annotation.ParsableArgument;
import ArgPar.Annotation.ParsableCommand;
import ArgPar.Annotation.ParsableFlag;
import ArgPar.Annotation.ParsableOption;
import ArgPar.Exception.ParseException;

public class ArgumentParser<Command> {
    private Class<Command> klass;

    public ArgumentParser(Class<Command> klass) {
        requireNonNull(klass);
        this.klass = klass;
    }

    public Command parse(String[] args) throws ParseException {
        try {
            var annotations = klass.getAnnotations();
            if (annotations.length == 0) {
                throw new IllegalArgumentException("Error: " + klass.toString() + " needs to have a `ParsableCommand` annotation!");
            }
            var annotation = annotations[0];
            if (!(annotation instanceof ParsableCommand)) {
                throw new IllegalArgumentException("Error: " + klass.toString() + " needs to have a `ParsableCommand` annotation!");
            }

            var instance = klass.getDeclaredConstructor().newInstance();

            var parsables = new ArrayList<Parsable>();
            for (var field : klass.getDeclaredFields()) {
                // @todo: - skip static fields

                if (field.getAnnotations().length == 0) {
                    continue;
                }

                Parsable pars = null;
                if (field.getAnnotations()[0] instanceof ParsableArgument) {
                    var arg = (ParsableArgument) field.getAnnotations()[0];
                    pars = Parsable.arg(arg, field, instance);
                }
                if (field.getAnnotations()[0] instanceof ParsableOption) {
                    var opt = (ParsableOption) field.getAnnotations()[0];
                    pars = Parsable.opt(opt, field, instance);
                }
                if (field.getAnnotations()[0] instanceof ParsableFlag) {
                    var flag = (ParsableFlag) field.getAnnotations()[0];
                    pars = Parsable.flag(flag, field, instance);
                }
                if (pars == null) {
                    continue;
                }
                parsables.add(pars);
            }

            var parser = new InternalParser((ParsableCommand) annotation, parsables);
            parser.parse(args);

            return instance;
        } catch (InstantiationException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (SecurityException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    private static Object parseValue(String value, Class<?> type) {
        Object val = null;
        try {
            if (type.equals(boolean.class) || type.equals(Boolean.class)) {
                val = Boolean.parseBoolean(value);
            } else if (type.equals(String.class)) {
                val = value;
            } else if (type.equals(int.class) || type.equals(Integer.class)) {
                val = Integer.parseInt(value);
            } else if (type.equals(double.class) || type.equals(Double.class)) {
                val = Double.parseDouble(value);
            } else if (type.equals(float.class) || type.equals(Float.class)) {
                val = Float.parseFloat(value);
                // @todo: - add support for additional types
            } else if (type.isArray()) {
                var split = value.split(",");
                var ct = type.componentType();
                var vals = (Object[]) type.cast(java.lang.reflect.Array.newInstance(ct, split.length));
                for (int i = 0; i < split.length; i++) {
                    vals[i] = parseValue(split[i], ct);
                }
                val = vals;
            } else {
                var methods = type.getMethods();
                for (var method : methods) {
                    var params = method.getParameters();
                    if (method.getName().equals("valueOf") && params.length == 1
                            && params[0].getType().equals(String.class)) {
                        val = method.invoke(null, value);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            throw new ParseException("Error: Cannot parse value of type '" + type.toString() + "'!");
        }
        if (val == null) {
            throw new ParseException("Error: Cannot parse value of type '" + type.toString() + "'!");
        }
        return val;
    }

    private abstract static class Parsable {
        Field field;
        Object instance;

        Parsable(Field field, Object instance) {
            this.field = field;
            this.instance = instance;
        }

        static Parsable arg(ParsableArgument arg, Field f, Object instance) {
            return new Argument(arg, f, instance);
        }

        static Parsable opt(ParsableOption opt, Field f, Object instance) {
            return new Option(opt, f, instance);
        }

        static Parsable flag(ParsableFlag flag, Field f, Object instance) {
            return new Flag(flag, f, instance);
        }

        abstract void parse(String[] args, Set<Integer> alreadyParsed);
    }

    private static class Option extends Parsable {
        ParsableOption option;

        Option(ParsableOption option, Field field, Object instance) {
            super(field, instance);
            this.option = option;
        }

        void parse(String[] args, Set<Integer> alreadyParsed) {
            var i = -1;
            for (var arg : args) {
                i++;
                if (alreadyParsed.contains(i)) {
                    continue;
                }

                for (var name : option.name()) {
                    if (!arg.startsWith(name)) {
                        continue;
                    }

                    Object val = null;
                    var isArgEqVal = arg.length() > name.length() && arg.charAt(name.length()) == '=';
                    if (isArgEqVal) {
                        // this arg is in form 'arg=val'
                        var splitByEq = arg.split("=");
                        val = parseValue(splitByEq[1], field.getType());
                    } else {
                        // this arg is in form 'arg val'
                        if (i + 1 >= args.length) {
                            throw new ParseException("Error: Missing argument for option '" + name + "'!");
                        }
                        val = parseValue(args[++i], field.getType());
                    }

                    try {
                        field.set(instance, val);
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException();
                    } catch (IllegalAccessException e) {
                        throw new IllegalArgumentException(
                                "This error most likely happened because fields are not marked as `public`! Mark them as such and try again.");
                    }

                    if (isArgEqVal) {
                        alreadyParsed.add(i);
                    } else {
                        alreadyParsed.add(i - 1);
                        alreadyParsed.add(i);
                    }
                    return;
                }
            }
            var name = option.name()[0];
            for (var n : option.name()) {
                if (n.length() > name.length()) {
                    name = n;
                }
            }
            try {
                if (field.get(instance) == null)
                    throw new ParseException("Error: Missing expected option '" + name + "'!");
                else {
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                System.exit(99);
            } catch (IllegalAccessException e) {
                System.exit(99);
            }
        }
    }

    private static class Argument extends Parsable {
        ParsableArgument arg;

        Argument(ParsableArgument arg, Field field, Object instance) {
            super(field, instance);
            this.arg = arg;
        }

        void parse(String[] args, Set<Integer> alreadyParsed) {
            for (int i = 0; i < args.length; i++) {
                var arg = args[i];

                if (alreadyParsed.contains(i)) {
                    continue;
                }
                if (arg.startsWith("-", 0)) {
                    i++;
                    continue;
                }

                var val = parseValue(arg, field.getType());
                try {
                    field.set(instance, val);
                    alreadyParsed.add(i);
                    return;
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException();
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(
                            "This error most likely happened because fields are not marked as `public`! Mark them as such and try again.");
                }
            }
            if (arg.defaultValue() != null && arg.defaultValue().length() > 0) {
                var parsed = parseValue(arg.defaultValue(), field.getType());
                try {
                    field.set(instance, parsed);
                    return;
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException();
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(
                            "This error most likely happened because fields are not marked as `public`! Mark them as such and try again.");
                }
            }
            throw new ParseException("Error: Missing expected argument <" + field.getName() + ">!");
        }
    }

    private static class Flag extends Parsable {
        ParsableFlag flag;

        Flag(ParsableFlag flag, Field field, Object instance) {
            super(field, instance);
            this.flag = flag;
        }

        @Override
        void parse(String[] args, Set<Integer> alreadyParsed) {
            var i = -1;
            for (var arg : args) {
                i++;
                if (alreadyParsed.contains(i)) {
                    continue;
                }

                for (var name : flag.name()) {
                    if (!arg.equals(name)) {
                        continue;
                    }
                    alreadyParsed.add(i);
                    try {
                        field.set(instance, true);
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException();
                    } catch (IllegalAccessException e) {
                        throw new IllegalArgumentException(
                                "This error most likely happened because fields are not marked as `public`! Mark them as such and try again.");
                    }
                    return;
                }
            }
            try {
                if (field.get(instance) == null)
                    throw new ParseException("Error: Missing expected flag '" + field.getName() + "'!");
            } catch (IllegalArgumentException e) {
                throw new ParseException("Error: Missing expected flag '" + field.getName() + "'!");
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(
                        "This error most likely happened because fields are not marked as `public`! Mark them as such and try again.");
            }
        }
    }

    private static class InternalParser {
        List<Parsable> parsables;
        ParsableCommand command;

        InternalParser(ParsableCommand command, List<Parsable> parsables) {
            requireNonNull(command);
            requireNonNull(parsables);
            this.command = command;
            this.parsables = parsables;
        }

        void parse(String[] args) {
            if (args.length == 0) {
                if (parsables.isEmpty()) {
                    return;
                }
                System.out.println("Error: Could not parse command!");
                System.out.println(commandHelp());
                throw new ParseException("Could not parse command!");
            }

            var offset = command.commandName().length() > 0 ? 1 : 0;
            var argcpy = new String[args.length - offset];
            System.arraycopy(args, offset, argcpy, 0, args.length - offset);

            Set<Integer> alreadyUsedArgs = new HashSet<>();

            if (args.length > 0 && command.commandName().length() > 0 && !args[0].equals(command.commandName())) {
                System.out.println("Error: Expected command '" + command.commandName() + "'!");
                System.out.println(commandHelp());
                throw new ParseException("Error: Expected command '" + command.commandName() + "'!");
            }

            try {
                for (var parsable : parsables) {
                    parsable.parse(argcpy, alreadyUsedArgs);
                }
            } catch (ParseException e) {
                System.out.println(e.toString());
                System.out.println(commandHelp());
                throw e;
            }

            if (argcpy.length != alreadyUsedArgs.size()) {
                System.out.println("Error: Unknown arguments given!");
                System.out.println(commandHelp());
                throw new ParseException("Error: Unknown arguments given!");
            }
        }

        String commandHelp() {
            var name = command.commandName();
            var version = command.version();
            var description = command.description();

            var builder = new StringBuilder();
            if (description != null && description.length() > 0) {
                builder.append("OVERVIEW: ");
                builder.append(description);
                builder.append("\n");
            }
            if (version != null && version.length() > 0) {
                builder.append("Version: ");
                builder.append(version);
                builder.append("\n");
            }
            builder.append("Usage: ");
            builder.append(name);
            builder.append(" ");

            for (var parsable : parsables) {
                if (parsable instanceof Argument) {
                    builder.append("<");
                    builder.append(((Argument) parsable).field.getName());
                    builder.append("> ");
                }
            }

            for (var parsable : parsables) {
                if (parsable instanceof Option) {
                    var option = (Option) parsable;
                    var optName = option.option.name()[0];
                    for (var n : option.option.name()) {
                        if (n.length() > name.length()) {
                            optName = n;
                        }
                    }
                    builder.append("[");
                    builder.append(optName);
                    builder.append(" ");
                    builder.append("<");
                    builder.append(optName.substring(optName.lastIndexOf('-') + 1));
                    builder.append(">");
                    builder.append("]");
                }
            }

            for (var parsable : parsables) {
                if (parsable instanceof Flag) {
                    var flag = (Flag) parsable;
                    var optName = flag.flag.name()[0];
                    for (var n : flag.flag.name()) {
                        if (n.length() > name.length()) {
                            optName = n;
                        }
                    }
                    builder.append("[");
                    builder.append(optName);
                    builder.append("]");
                }
            }

            return builder.toString();
        }
    }
}

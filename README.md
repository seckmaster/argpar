# ArgPar

## Declarative argument parsing library

ArgPar is a declarative argument parsing library based on annotations. It enables you to build complex CLIs with ease.

## Examples:

A single argument CLI:

```java
@ParsableCommand(commandName = "Test1", description = "Command Line Application")
public static class Test1 { 
    @ParsableArgument
    public String name;
}
```

Usage:

```java
var parser = new ArgumentParser<Test1>(Test1.class);
{
    var cmd = parser.parse(args("Test1 test"));
    assertEqual("test", cmd.name);
}
{
    var cmd = parser.parse(args("Test1 arg"));
    assertEqual("arg", cmd.name);
}
{
    // incorrent input:
    var cmd = parser.parse(args("Test"));
    // ArgPar generates a help string automatically and prints it to stdout
}
```

A two-argument CLI:

Argument `counter` is given a default value. Note that its type is always `String`; the value gets
converted at runtime. An exception is thrown if conversion fails.

```java
@ParsableCommand(commandName = "Test2", description = "Command Line Application")
public static class Test2 { 
    @ParsableArgument
    public String name;

    @ParsableArgument(defaultValue = "10")
    public int counter;
}
```

Usage:

```java
var parser = new ArgumentParser<Test2>(Test2.class);
{
    var cmd = parser.parse(args("Test2 test"));
    assertEqual("test", cmd.name);
    assertEqual(10, cmd.counter); // default value is used
}
{
    var cmd = parser.parse(args("Test2 arg 20"));
    assertEqual("arg", cmd.name);
    assertEqual(20, cmd.counter);
}
```

A CLI with an argument and an option:

Options can be parsed in two ways, as shown bellow.

```java
@ParsableCommand(commandName = "Test3", description = "Command Line Application")
public static class Test3 { 
    @ParsableArgument
    public boolean arg;

    @ParsableOption(name = "--flag")
    public Boolean flag;
}
```

Usage:

```java
var parser = new ArgumentParser<Test3>(Test3.class);
{
    // incorrent input:
    var cmd = parser.parse(args("Test3 test"));
}
{
    // option can be parsed as a pair of arguments ...
    var cmd = parser.parse(args("Test3 true --flag false"));
    assertEqual(true, cmd.arg);
    assertEqual(false, cmd.flag);
}
{
    // ... or as in '--option=value'
    var cmd = parser.parse(args("Test3 true --flag=true"));
    assertEqual(true, cmd.arg);
    assertEqual(true, cmd.flag);
}
{
    // option(s) can precede arguments
    var cmd = parser.parse(args("Test3 --flag true false"));
    assertEqual(false, cmd.arg);
    assertEqual(true, cmd.flag);
}
```

Options can have multiple __names__:

```java
@ParsableCommand(commandName = "Test4", description = "Command Line Application")
public static class Test4 { 
    @ParsableOption(name = {"--flag", "-f"})
    public Integer flag;
}
```

Usage:

```java
var parser = new ArgumentParser<Test4>(Test4.class);
{
    var cmd = parser.parse(args("Test4 --flag 0"));
    assertEqual(0, cmd.flag);
}
{
    var cmd = parser.parse(args("Test4 -f 5555"));
    assertEqual(5555, cmd.flag);
}
{
    var cmd = parser.parse(args("Test4 -f=5555"));
    assertEqual(5555, cmd.flag);
}
```

ArgPar is able to decode enum types:

```java
@ParsableCommand(commandName = "Test5", description = "Command Line Application")
public static class Test5 { 
    public static enum Mode { A, B }

    @ParsableOption(name = "-m")
    public Mode mode;
}
```

Usage:

```java
var parser = new ArgumentParser<Test5>(Test5.class);
{
    var cmd = parser.parse(args("Test5 -m A"));
    assertEqual(Test5.Mode.A, cmd.mode);
}
{
    var cmd = parser.parse(args("Test5 -m B"));
    assertEqual(Test5.Mode.B, cmd.mode);
}
```

CLI with flags:

A flag is a dash-prefixed label that can be provided on the command line, such as `-d` and `--debug`.

```java
@ParsableCommand(commandName = "Test6", description = "Command Line Application")
public static class Test6 {
    @ParsableFlag(name = "--flag1")
    public boolean flag1; // default value is `false`

    @ParsableFlag(name = {"--flag2", "-f2"}) // flags can have multiple names
    public boolean flag2; // default value is `false`
}
```

Usage:

```java
var parser = new ArgumentParser<Test6>(Test6.class);
{
    var cmd = parser.parse(args("Test6"));
    assertEqual(false, cmd.flag1);
    assertEqual(false, cmd.flag2);
}
{
    var cmd = parser.parse(args("Test6 --flag1"));
    assertEqual(true, cmd.flag1);
    assertEqual(false, cmd.flag2);
}
{
    var cmd = parser.parse(args("Test6 --flag2"));
    assertEqual(false, cmd.flag1);
    assertEqual(true, cmd.flag2);
}
{
    var cmd = parser.parse(args("Test6 -f2"));
    assertEqual(false, cmd.flag1);
}  
{
    var cmd = parser.parse(args("Test6 --flag1 --flag2"));
    assertEqual(true, cmd.flag1);
    assertEqual(true, cmd.flag2);
}
{
    var cmd = parser.parse(args("Test6 --flag2 --flag1"));
    assertEqual(true, cmd.flag1);
    assertEqual(true, cmd.flag2);
}
{
    var cmd = parser.parse(args("Test6 -f2 --flag1"));
    assertEqual(true, cmd.flag1);
    assertEqual(true, cmd.flag2);
}
```

ArgPar is able to decode Array types as well. The limitation being 
that only arrays of boxed types are supported (`Integer[]` is supported,
`int[]` not).

```java
@ParsableCommand(commandName = "Test8", description = "")
public static class Test8 {
    @ParsableOption(name = "--nums")
    public Integer[] numbers;
}
```

Array of values is given as a comma separated list. Usage:

```java
var parser = new ArgumentParser<Test8>(Test8.class);
{
    var cmd = parser.parse(args("Test8 --nums 1,2,3,4,5"));
    Integer[] expected = {1,2,3,4,5};
    assertEqualArray(expected, cmd.numbers);
}
{
    var cmd = parser.parse(args("Test8 --nums=1,2,3,4,5"));
    Integer[] expected = {1,2,3,4,5};
    assertEqualArray(expected, cmd.numbers);
}
```

ArgPar enables decoding of custom types as well. For instance, we might
want to define an enum and wish to decode a list of its values contained
in a `EnumSet` for fast lookup. This can be achieved by implementing a `valueOf`
method which accepts a `String` and returns an instance of the type. Make sure that
this method is `public` and `static`.

```java
@ParsableCommand(commandName = "Test10", description = "")
public static class Test10 {
    public static class PhasesEnumSet extends ForwardingSet<Phase> {
        PhasesEnumSet(EnumSet<Phase> set) {
            super(set);
        }

        // The method which is invoked by ArgPar to decode the argument
        public static PhasesEnumSet valueOf(String arg) {
            var split = arg.split(",");
            var set = new PhasesEnumSet(EnumSet.noneOf(Phase.class));
            for (var s : split) {
                var phase = Phase.valueOf(s.trim());
                if (phase == null) {
                    throw new IllegalArgumentException("Could not parse <Phase>!");
                }
                set.add(phase);
            }
            return set;
        }
    }

    public static enum Phase {
        LEX, SYN, AST, SEM, FRM, IMC, INT
    }

    @ParsableOption(name = "--phases")
    public PhasesEnumSet phases;
}
```

Usage:

```java
var parser = new ArgumentParser<Test10>(Test10.class);
{
    var cmd = parser.parse(args("Test10 --phases LEX,SYN,IMC"));
    assertEqual(true, cmd.phases.contains(Test10.Phase.LEX));
    assertEqual(true, cmd.phases.contains(Test10.Phase.SYN));
    assertEqual(true, cmd.phases.contains(Test10.Phase.IMC));
    assertEqual(false, cmd.phases.contains(Test10.Phase.FRM));
    assertEqual(false, cmd.phases.contains(Test10.Phase.AST));
    assertEqual(false, cmd.phases.contains(Test10.Phase.SEM));
    assertEqual(false, cmd.phases.contains(Test10.Phase.INT));
}
{
    var cmd = parser.parse(args("Test10 --phases SYN"));
    assertEqual(true, cmd.phases.contains(Test10.Phase.SYN));
    assertEqual(false, cmd.phases.contains(Test10.Phase.LEX));
    assertEqual(false, cmd.phases.contains(Test10.Phase.IMC));
    assertEqual(false, cmd.phases.contains(Test10.Phase.FRM));
    assertEqual(false, cmd.phases.contains(Test10.Phase.AST));
    assertEqual(false, cmd.phases.contains(Test10.Phase.SEM));
    assertEqual(false, cmd.phases.contains(Test10.Phase.INT));
}
```

`Command` can be grouped into a `CommandGroup`. This way we can build a nice CLI:

```java
@CommandGroup(name = "Group", description = "Command Line Interface", subcommands = {Test5.class, Test6.class})
public static class CLI {
}
```

Usage:

```java
var parser = new ArgumentGroupParser(Test7.class);
{
    // incorrect inputs:
    parser.parse(args(""), Object.class);
    parser.parse(args("Test5"), Object.class);
    parser.parse(args("Test5 --flag 100"), Object.class);
}
{
    var cmd = parser.parse(args("Test5 --flag 100 -b false --num 12 -m A"), Test5.class);
    assertEqual(100, cmd.flag);
    assertEqual(false, cmd.build);
    assertEqual(12, cmd.num);
    assertEqual(Test5.Mode.A, cmd.mode);
}
{
    var cmd = parser.parse(args("Test6"), Test6.class);
    assertEqual(false, cmd.flag1);
    assertEqual(false, cmd.flag2);
}
{
    var cmd = parser.parse(args("Test6 --flag1"), Test6.class);
    assertEqual(true, cmd.flag1);
    assertEqual(false, cmd.flag2);
}
```
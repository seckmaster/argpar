jar:
	mkdir src/.out/
	cd src/ && find ArgPar/ -type f -name "*.java" | xargs javac -d .out/ && cd .out && find ArgPar/ -type f -name "*.class" | xargs jar cf ../../builds/ArgPar.jar
	rm -rf src/.out/

